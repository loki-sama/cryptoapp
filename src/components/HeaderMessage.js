import React from "react";
import {
  Button,
  Container,
  Grid,
  Header,
  Icon,
  Image,
  Segment,
  Visibility
} from "semantic-ui-react";

const HeaderMessage = ({
  minHeight = 700,
  title = "WE'RE CRYPTO APP-",
  description = "THE CRYPTO EXPERTS"
}) => (
  <div>
    <Segment
      inverted
      textAlign="center"
      style={{ minHeight: minHeight, padding: "1em 0em" }}
      vertical
    >
      <Container text>
        <Header
          as="h1"
          content={title}
          inverted
          style={{
            fontSize: "3em",
            fontWeight: "normal",
            marginBottom: 0,
            marginTop: "4em"
          }}
        />
        <Header
          as="h1"
          content={description}
          style={{
            fontSize: "3em",
            fontWeight: "normal",
            color: "#d45c54"
          }}
        />
      </Container>
    </Segment>
  </div>
);

export default HeaderMessage;
