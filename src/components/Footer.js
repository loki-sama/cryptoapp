import React from "react";
import Link from "gatsby-link";
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility
} from "semantic-ui-react";
import github from "../img/github-icon.svg";
import logo from "../img/logo.svg";

const Navbar = () => (
  <Segment inverted vertical style={{ padding: "5em 0em" }}>
    <Container>
      <Grid divided inverted stackable>
        <Grid.Row>
          <Grid.Column width={6}>
            <Header inverted as="h4" content="Location" />
            <List link inverted>
              <List.Item as="a">CRYPTO APP</List.Item>
              <List.Item as="a">Berlin</List.Item>
              <List.Item as="a">...</List.Item>
              <List.Item as="a">map</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={6}>
            <Header inverted as="h4" content="Reach us" />
            <List link inverted>
              <List.Item as="a">Git</List.Item>
              <List.Item as="a">Twitter</List.Item>
              <List.Item as="a">Facebook</List.Item>
              <List.Item as="a">Redit</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={4} textAlign="right" verticalAlign="bottom">
            
            
            <Icon name="copyright" /> Crypto App
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  </Segment>
);

export default Navbar;
