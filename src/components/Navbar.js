import React from "react";
import Link from "gatsby-link";
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility
} from "semantic-ui-react";
import github from "../img/github-icon.svg";
import logo from "../img/logo.svg";

const Navbar = () => (
  <Segment inverted textAlign="center" vertical>
    <Menu pointing size="large" inverted secondary>
      <Menu.Item as={Link} to="/" header style={{ fontSize: "2em" }}>
        CRYPTO APP
      </Menu.Item>
      <Menu.Menu
        position="right"
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Menu.Item as={Link} to="/" fitted="vertically">
          Home
        </Menu.Item>
        <Menu.Item as={Link} to="/team">
          Team
        </Menu.Item>
        <Menu.Item as={Link} to="/whitepaper">
          White Paper
        </Menu.Item>
        <Menu.Item as={Link} to="/blog">
          Blog
        </Menu.Item>
      </Menu.Menu>
    </Menu>
  </Segment>
);

export default Navbar;
