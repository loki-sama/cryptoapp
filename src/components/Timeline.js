import React, { Component } from "react";
import PropTypes from "prop-types";

class Timeline extends Component {
  style = {
    width: "95%",
    maxWidth: "1170px",
    margin: "2em auto",
    position: "relative",
    padding: "2em 0"
  };

  render() {
    const { animate, children } = this.props;
    let { className } = this.props;

    className += " vertical-timeline";

    if (animate) {
      className += " vertical-timeline--animate";
    }

    return (
      <div>
        <span
          style={{
            content: "",
            position: "absolute",
            top: "0",
            left: "18px",
            height: "100%",
            width: "4px",
            background: "white"
          }}
        />
        <div style={style}>{children}</div>
        <span
          style={{
            content: "",
            display: "table",
            clear: "both"
          }}
        />
      </div>
    );
  }
}

VerticalTimeline.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  animate: PropTypes.bool
};

VerticalTimeline.defaultProps = {
  animate: true,
  className: ""
};

export default VerticalTimeline;
