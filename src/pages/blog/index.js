import React from "react";
import graphql from "graphql";
import {
  Image,
  Icon,
  Button,
  Segment,
  Container,
  Grid,
  Header
} from "semantic-ui-react";
import Link from "gatsby-link";
import Script from "react-load-script";
import HeaderMessage from "../../components/HeaderMessage";

export default class BlogPage extends React.Component {
  render() {
    const { data } = this.props;
    const { edges: posts } = data.allMarkdownRemark;

    return (
      <div>
        <HeaderMessage
          title="OUR BLOG -"
          description="EXPERT OPINIONS"
        />
     

        {posts
          .filter(post => post.node.frontmatter.templateKey === "blog-post")
          .map(({ node: post }) => (
            <Segment style={{ padding: "8em 0em" }} vertical>
              <Grid container stackable verticalAlign="middle">
                <Grid.Row>
                  <Grid.Column width={4}>
                    <Header as="h3" style={{ fontSize: "2em" }}>
                      {post.frontmatter.title}
                    </Header>
                  </Grid.Column>
                  <Grid.Column floated="right" width={12}>
                    <p style={{ fontSize: "1.33em" }}>{post.excerpt}</p>
                    <p style={{ fontSize: "1.33em" }}>
                      If you have a big idea, we’ll help design the UX and UI,
                      then craft your minimum viable product. If you need a few
                      more hands to get the job done, we’ll augment your team
                      with experienced engineers. And if you want to level up
                      your whole organization, we’ll lead on-site training
                      tailored to your needs.
                    </p>
                    <Button
                      as={Link}
                      to={post.frontmatter.path}
                      size="huge"
                      style={{ backgroundColor: "#d45c54", color: "white" }}
                    >
                      Keep Reading →
                    </Button>
                   
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column textAlign="center" />
                </Grid.Row>
              </Grid>
            </Segment>
          ))}
      </div>
    );
  }
}

export const pageQuery = graphql`
  query BlogQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
            path
          }
        }
      }
    }
  }
`;
