import React from "react";
import Link from "gatsby-link";
import Script from "react-load-script";
import graphql from "graphql";
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility,
  Embed
} from "semantic-ui-react";
import HeaderMessage from "../components/HeaderMessage";
import Countdown from "../components/Countdown";

export default class IndexPage extends React.Component {
  handleScriptLoad() {
    if (window.netlifyIdentity) {
      window.netlifyIdentity.on("init", user => {
        if (!user) {
          window.netlifyIdentity.on("login", () => {
            document.location.href = "/admin/";
          });
        }
      });
    }
    window.netlifyIdentity.init();
  }

  render() {
    const { data } = this.props;
    const { edges: posts } = data.allMarkdownRemark;

    return (
      <div>
        <Script
          url="https://identity.netlify.com/v1/netlify-identity-widget.js"
          onLoad={() => this.handleScriptLoad()}
        />
        <HeaderMessage />
        <Container style={{ marginTop: 40 }} />
        <Segment style={{ padding: "8em 0em" }} vertical>
          <Grid container stackable verticalAlign="middle">
            <Grid.Row>
              <Grid.Column width={4}>
                <Header as="h3" style={{ fontSize: "2em" }}>
                  The Clock is ticking for Crypto App Ico
                </Header>
              </Grid.Column>
              <Grid.Column width={12}>
                <Countdown date={1517944887231} />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column floated="right" width={12}>
                <p style={{ fontSize: "1.33em" }}>
                  Formidable is a Seattle and London-based engineering
                  consultancy and open source software organization,
                  specializing in React.js, React Native, GraphQL, Node.js, and
                  the extended JavaScript ecosystem.
                </p>
                <p style={{ fontSize: "1.33em" }}>
                  Since 2013, our agile team has worked with companies ranging
                  in size from startups, to Fortune 100s, to build quality
                  software and level up engineering teams.
                </p>
                <p style={{ fontSize: "1.33em" }}>
                  Launching a new JavaScript Product? Need Help with an Existing
                  Project?{" "}
                </p>
                <Button
                  size="huge"
                  style={{ backgroundColor: "#d45c54", color: "white" }}
                >
                  Check Us Out
                </Button>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
        <Segment style={{ padding: "8em 0em" }} vertical>
          <Grid container stackable verticalAlign="middle">
            <Grid.Row>
              <Grid.Column width={4}>
                <Header as="h3" style={{ fontSize: "2em" }}>
                  A Crypto App Built for the Modern Web{" "}
                </Header>
              </Grid.Column>
              <Grid.Column floated="right" width={12}>
                <p style={{ fontSize: "1.33em" }}>
                  Formidable is a Seattle and London-based engineering
                  consultancy and open source software organization,
                  specializing in React.js, React Native, GraphQL, Node.js, and
                  the extended JavaScript ecosystem.
                </p>
                <p style={{ fontSize: "1.33em" }}>
                  Since 2013, our agile team has worked with companies ranging
                  in size from startups, to Fortune 100s, to build quality
                  software and level up engineering teams.
                </p>
                <p style={{ fontSize: "1.33em" }}>
                  Launching a new JavaScript Product? Need Help with an Existing
                  Project?{" "}
                </p>
                <Button
                  size="huge"
                  style={{ backgroundColor: "#d45c54", color: "white" }}
                >
                  Check Them Out
                </Button>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign="center" />
            </Grid.Row>
          </Grid>
        </Segment>

        <Segment style={{ padding: "8em 0em" }} vertical>
          <Grid container stackable verticalAlign="middle">
            <Grid.Row>
              <Grid.Column width={4}>
                <Header as="h3" style={{ fontSize: "2em" }}>
                  Check out
                </Header>
              </Grid.Column>
              <Grid.Column floated="right" width={12}>
                <Embed hd id="O6Xo21L0ybE" source="youtube" />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>

      </div>
    );
  }
}

export const pageQuery = graphql`
  query IndexQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
            path
          }
        }
      }
    }
  }
`;
