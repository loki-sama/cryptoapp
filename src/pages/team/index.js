import React from "react";
import graphql from "graphql";
import { Card, Image, Icon, Button, Segment, Grid, Header } from "semantic-ui-react";
import Link from "gatsby-link";
import Script from "react-load-script";
import HeaderMessage from "../../components/HeaderMessage";

export default class TeamPage extends React.Component {
  render() {
    console.log(this.props);
    const data = this.props.data.allMarkdownRemark.edges;
    console.log(data);
    return (
      <div>
        <HeaderMessage
          title="OUR TEAM-"
          description="EXPERT SOLUTIONS FOR CRYPTO TASKS"
        />
        <Segment style={{ padding: "8em 0em" }} vertical>
          <Grid container stackable verticalAlign="middle">
            <Grid.Row>
              <Grid.Column width={4}>
                <Header as="h3" style={{ fontSize: "2em" }}>
                  We’re a team of Engineers, Leaders, and Advocates.
                </Header>
              </Grid.Column>
              <Grid.Column floated="right" width={12}>
                <p style={{ fontSize: "1.33em" }}>
                  Formidable is a Seattle-based consultancy and open-source
                  shop, with an emphasis on Node.js and React.js. We deploy a
                  mixture of consulting, staff augmentation, and training to
                  level up teams and solve engineering problems. Whether it’s
                  transitioning walmart.com to React, moving speedtest.net off
                  Flash, or helping a startup build and scale an MVP, we’re
                  ready to help teams of any size.
                </p>
                <p style={{ fontSize: "1.33em" }}>
                  If you have a big idea, we’ll help design the UX and UI, then
                  craft your minimum viable product. If you need a few more
                  hands to get the job done, we’ll augment your team with
                  experienced engineers. And if you want to level up your whole
                  organization, we’ll lead on-site training tailored to your
                  needs.
                </p>
              
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign="center" />
            </Grid.Row>
          </Grid>
        </Segment>
        <Card.Group itemsPerRow={4}>
          {data
            .filter(post => post.node.frontmatter.templateKey === "team-page")
            .map(({ node: post }) => (
              <Card key={post.id}>
                <Image src={post.frontmatter.image} />
                <Card.Content>
                  <Card.Header>{post.frontmatter.name}</Card.Header>
                  <Card.Description>
                    {post.frontmatter.description}
                  </Card.Description>
                  <Button circular animated as={Link} to="http://github.io">
                    <Button.Content visible>
                      <Icon size="large" name="github" link />
                    </Button.Content>
                    <Button.Content hidden>
                      <Icon name="right arrow" />
                    </Button.Content>
                  </Button>
                </Card.Content>
              </Card>
            ))}
        </Card.Group>
      </div>
    );
  }
}

export const pageQuery = graphql`
  query TeamQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 400)
          id
          frontmatter {
            templateKey
            image
            name
            description
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
    }
  }
`;
