
import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import './all.sass';
import 'semantic-ui-css/semantic.min.css';


const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet title="Home | Crypto App" />
    <Navbar />
    <div>{children()}</div>
    <Footer />
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func,
};

export default TemplateWrapper;
